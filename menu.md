# our menu!
### Massa man Curry
* 500 g chicken thigh or breast. (aoyzii)
* 150 g of roasted peanuts. (aoyzii)
* Coconut milk 250 g. (aoyzii)
* Massaman curry paste 150 g. (aoyzii)
* 2 tbsp. brown sugar.
* 1/2 yellow onion, diced.
* 3 carrots, peeled and sliced into 1/8-inch thick slices.
* 3 russet potatoes, peeled and cut into 1/2-inch cubes.
* 50 g Potato, cut in cubes (boiled) (P)
* 30 g Onion, cut in cubes (P)

### Spicy curry
* duck grill 100 gram
* red chilli 20 gram
* fish ball 150 gram
* salt 1 gram
* Spring onion 10 g. (J)
* pork 10 g. (J)
* chilli 10 g. (J)
* garlic 10 g. (J)
* 2 spoon Curry (aoyzii)
* 500 gram pork (aoyzii)
* Coconut milk 250 g. (aoyzii)
* 1 tbsp vegetable oil
* ½ small pack Thai basil
* 1 tsp brown sugar
* 2 tbsp fish sauce
* finely chopped 1 large white onion (P)
* broken into pieces ½ (2 inch) stick cinnamon stick (P)
* leaves 4 bay (P)

### Jelly noodle soup
* mushroom 100 gram
* galic 10 gram
* fish sauce 2 spoons
* phak kat khao 150 gram
* Vermicelli soaked in water until soft short cut to 100 grams. (aoyzii)
* pepper grain 10 tablets (aoyzii)
* Spring onion celery 1-2 haulm. (aoyzii)
* 1 tbsp Chinese black vinegar
* 2 cloves of garlic
* 2 spring onions, sliced diagonally
* 4 and 1/2 cup water, divided (P)
* sugar 1 tablespoon (P)

### Sticky Rice with Mango
* coconut milk 1 kg. (J)
* Sugar 10 g. (J)
* Salt 10 g. (J)
* Sticky rice 5 kg. (J)
* 500 g. sweet mango (aoyzii)
* sticky rice 0.5 kg. (aoyzii)
* 1/3 cup plus 3 tablespoons sugar.
* 1 tablespoon sesame seeds, toasted lightly.
*  yellow mung beans 100 g. (P)
#### How to
1. In a bowl wash rice well in several changes of cold water until water is clear. Soak rice in cold water to cover overnight.
2. Drain rice well in a sieve. Set sieve over a large deep saucepan of simmering water (sieve should not touch water) and steam rice, covered with a kitchen towel and a lid, 30 to 40 minutes, or until tender (check water level in pan occasionally, adding more water if necessary).
3. While rice is cooking, in a small saucepan bring 1 cup coconut milk to a boil with 1/3 cup sugar and salt, stirring until sugar is dissolved, and remove from heat. Keep mixture warm.
4. Transfer cooked rice to a bowl and stir in coconut-milk mixture. Let rice stand, covered, 30 minutes, or until coconut-milk mixture is absorbed. Rice may be prepared up to this point 2 hours ahead and kept covered at room temperature.
5. While rice is standing, in cleaned small pan slowly boil remaining 1/3 cup coconut milk with remaining 3 tablespoons sugar, stirring occasionally, 1 minute. Transfer sauce to a small bowl and chill until cool and thickened slightly.
    
    To serve, mold 1/4 cup servings of sticky rice on dessert plates. Drizzle desserts with sauce and sprinkle with sesame seeds. Divide mango slices among plates.

### Hainanese chicken rice
* chicken blood 10 gram
* chicken liver 20 gram
* coriander 5 gram
* chicken 500 g. (aoyzii)
* soup 1/2 cup (aoyzii)
* rice 500g. (aoyzii)
* 2 garlic (aoyzii)
* 2 tablespoons cooking oil like canola, vegetable, peanut.
* 2 cloves garlic finely minced.
* 1 inch section of ginger finely minced (or grated on microplane grater.
* salt 3 teaspoons  (P)
* water 4 qt (P)

### Spicy Green Papaya Salad
* Garlic 10 g. (J)
* Sugar 10 g. (J)
* Fish Sauce 20 g. (J)
* Tamarind juice 20 g. (J)
* papaya 300 g. (aoyzii)
* chilli 10g. (aoyzii)
* 6 medium dried shrimp.
* 2 small Thai chiles, coarsely chopped.
#### How to Papaya Salad 
1. โขลกพริกกับกระเทียม 
2. ผ่ามะเขือเทศ และมะกอกลงไป 
3. ใส่น้ำตาลปี๊บ 
4. ใส่น้ำมะขามเปียก 
5. ใส่น้ำปลา น้ำมะนาว และน้ำปลาร้า 
6. คลุกให้เข้ากัน และตามด้วยมะละกอ
7. ตำให้น้ำเข้าเส้นแต่อย่าตำแรงจนเส้นเละจะไม่อร่อย 
8. ตักขึ้นใส่จาน เสิร์ฟคู่กับผักกาดและถั่วฝักยาว


### Pad thai
* Pickle turnip 10 gram
* tamarind water 10 gram
* tufu 20 gram
* Spring onion 50 gram
* Vegetable oil 100 ml. (J)
* Egg 20 g. (J)
* Salt 10 g. (J)
* Sugar 10 g. (J)
* noodles 500g. (aoyzii)
* water 100 ml (aoyzii)
* 3 cloves garlic , minced.
* 2 limes (P)

#### How to
1. นำมะขามเปียกมาแช่นำปลา แล้วใส่น้ำตาลปีบลงไปเพื่อเป็นน้ำปรุงรส หลังจากนั้นนำขึ้นตั้งไฟให้เดือดยกพักเอาไว้ Tamarind juice, fish sauce and Plam'sugar into a sauce and then boil until hot.

2. เอาน้ำมันพืชใส่กระทะ แล้วเทหอมที่ซอยไว้แล้ว เจียวจนมีกลิ่นหอม Put oil to the pan, the oil pan a bit hot, put onion slice and fried.

3. เอากุ้งแห้ง เต้าหู้เทใส่ลงไปผัด แล้วเอาเส้นเล็กใส่ และใข่ไก่ใส่ตามลงไป ตักน้ำปรุงที่เตรียมไว้แล้วใส่ลงไปผัดให้เข้ากันพอสุก แล้วปิดไฟเอาผักต่างๆ ใส่ตามลงไปแล้วคลุกเคล้าให้เข้ากัน Put dried shrimp, Tofu, noodl, egg and then put the sauce mix it and fried untill cooked, then put sping oinion and bean sprouts and tak off.

4. ตักขึ้นใส่จานเสริฟพร้อมถัวงอกดิบและมานาว. Save with remain fresh vegetables and lime.

### Noodles
* Salt 10 g. (J)
* Sugar 10 g. (J)
* deep fried garlic 20 g. (J)
* Liver 20 g. (J)
* Japanese noodles 500g. (aoyzii)
* water 500 ml (aoyzii)
* 0.5 spoon salt (aoyzii)
* 1 teaspoon vegetable oil or olive oil.
* 1/3 cup pork.
* 2 to 2-1/4 cups all-purpose flour. (P)
* 2 egg yolks and 1 whole egg, lightly beaten.(P)

### Omelet
* butter 15 gram
* origano 10 gram
* tomato 10 gram.
* bacon 20 gram
* 3 eggs (aoyzii)
* 2 spoon vegetable oil (aoyzii)
* 2 garlics (aoyzii)
* 100 g. of Cha-om (aoyzii)
* 1/2 cup milk
* 1/2 teaspoon salt.
* 2 tbsp. water (P)
* pepper 1 Dash (P)

### pork steak
* soy sauce 50 gram
* sugar 20 gram
* pork 1 kg
* black pepper 20 gram
* Salt 10 g. (J)
* Oyster sauce 10 g. (J)
* pork 500g. (aoyzii)
* 0.25 spoon vegetable oil (aoyzii)
* 1/4 cup butter
* 2 cloves garlic, minced
* 1/2 cup calamansi or lemon juice.(P)
* 1 large onion, peeled and sliced thinly.(P)

### fired rice
* Vegetable oil 100 ml. (J)
* Garlic 3 g. (J)
* carrot 3 g. (J)
* Oyster sauce 10 g. (J)
* streamed rice (aoyzii)
* pork 200g. (aoyzii)
* 2 eggs (aoyzii) 
* Minced white onion.
* Diced carrots.
* Green peas, fresh or frozen (avoid canned; too mushy)
* 3 tablespoons butter, divided (P)

### grilled chicken
* paprika powder 1 gram
* ketchup 200 gram
* chilli sauce 200
* salt 2 gram
* 500g chicken (aoyzii)
* 0.25 spoon of salt (aoyzii)
* 0.25 spoon of sugar (aoyzii)
* 1 spoon of Oyster sauce (aoyzii)
* 1/4 cup balsamic vinegar.
* Juice of 1 lemon.
* 2 tablespoons olive oil.
* 2 tablespoons Dijon mustard.
#### How to grilled chicken
* 1 หมักไก่ ใส่ Sauce ใส่เกลือ  ใส่น้ำมัน 30 นาที
* 2 ตั้งเตาถ่านไฟแรง ๆ
* 3 เอาไก่ไปย่าง ให้สุก
* 4 โรยผงปาปริก้า

### Khao Pad Kung
* Vegetable oil 100 ml. (J)
* Garlic 3 g. (J)
* Carrot 3 g. (J)
* Oyster sauce 10 g. (J)
* streamed rice (aoyzii)
* 3 shrimp (aoyzii)
* 1 spoon sugar (aoyzii)
* Minced white onion.
* White rice, jasmine or long-grain; fresh or leftover.
* Diced carrots.
* 1/4 cup shrimp
* 2 eggs (P)
* 1 red bell pepper (P)
#### How to:
1. เริ่มต้นด้วยการตั้งกระทะไฟแรง ใส่น้ำมันลงไป พอเริ่มร้อนใส่กระเทียมสับลงไปเจียวให้เหลือง
2. จากนั้นจึงใส่กุ้งลงไปผัดพอสุก
3. เสร็จแล้วใส่ข้าวสวยลงผัดให้ส่วนผสมเข้ากัน จึงใส่แครอทลงไปผัด 
4. ปรุงด้วยซีอิ๊วขาว น้ำมันหอย และพริกไทย ผัดให้เข้ากัน
5. ใช้ตะหลิวดันข้าวผัดขึ้นไปอยู่ข้าง ๆ กระทะ เว้นที่ก้นกระทะไว้ แล้วตอกไข่ใส่ลงไป ใช้ตะหลิวตีไข่ให้แตก จากนั้นตักข้าวที่อยู่ข้างกระทะกลบทับไข่ไว้ รอจนไข่สุกแล้วจึงช้อนกลับให้ไข่อยู่ด้านบน ผัดให้เข้ากัน ปิดไฟ ใส่ต้นหอมซอยลงไปคลุกเคล้าให้เข้ากันอีกที ยกลงจากเตาได้เลย 
6. ตักใส่จาน เสิร์ฟพร้อมแตงกวา ต้นหอม และมะนาว